import http from "@/utils/request"
//期望课程数据

export const expectDataApi = () => http.get("/data/expect.json");

//折线图数据

export const lineDataApi = () => http.get("/data/rate.json");

//任务进度数据

export const taskDataApi = () => http.get("/data/tarsk.json");
