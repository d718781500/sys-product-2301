import http from "@/utils/request"


//获取后台菜单数据的方法


export const getMenuListApi = () => http.get('/menus/getRoleMenus?order_by=1&type=1')


//获取所有的角色

export const getRoleListApi = (params = {}) => http.get('/role/getRole', { params })

//获取所有的权限

export const getPermissionListApi = (params = {}) => http.get('/permission/getPermissionPath', { params })


//获取当前角色的权限

export const getCurrentRolePermissionListApi = (params = {}) => http.get('/role/getRolePermission', { params })

//授权操作

/**
 * 
 * @param {} data rolieids:[]string 目标角色的id permissionids:[]string 目标权限的id集合
 * @returns 
 */

export const grantRoleApi = ({ roleids = [], permission_ids = [] }) => http.post('/role/grantRole', {
    roleids,
    permission_ids
})

