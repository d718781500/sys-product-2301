import http from "@/utils/request"


// 获取角色列表

export const getRoleApi = (params = { type: "1" }) => http.get('/role/getRole', { params })

//修改角色

export const updateRoleApi = (params = {}) => http.post('/role/updateRole', params) 

//分配角色

export const assignmentRoleApi = (params = {}) => http.post('role/roleAssignment', params)