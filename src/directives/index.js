//引入userStore

import useUser from "@/store/users"

//引入角色store

import useRole from "@/store/role"
import isChild from "@/utils/isChild"


let vAccess = {
    updated(el, binding) {
        // console.log(el)
        let userStore = useUser()

        let roleStore = useRole();
        // console.log(binding)
        //当前角色id
        let currentRoleid = userStore.userInfo.value.roleid
        //目标角色id
        let targetRoleid = binding.value;
        //角色列表
        let roleList = roleStore.allRoles;

        // console.log(targetRoleid)
        // console.log('---current',currentRoleid)
        // console.log(roleList)

        let isChildRole = isChild(currentRoleid, targetRoleid, roleList);

        // console.log(isChildRole)

        //保存当前按钮的类名

        let className = el.className;

        if (!isChildRole) {
            //说明是父级角色
            el.className = className + ' is-disabled'
            //禁用按钮
            el.disabled = true;

            //禁用switch
            if (el.querySelector('.el-switch__core')) {
                el.querySelector('.el-switch__input').setAttribute('disabled', 'disabled');
                el.querySelector('.el-switch__core').classList.add('is-disabled')

            }

        }


        // if (children.find(item => item.roleid == targetRoleid)) {
        //     el.className = className + ' is-disabled'
        //     //禁用按钮
        //     el.disabled = true;
        // }

    },
    mounted(el, binding) {

        let userStore = useUser()

        let roleStore = useRole();
        // console.log(binding)
        //当前角色id
        let currentRoleid = userStore.userInfo.value.roleid
        //目标角色id
        let targetRoleid = binding.value;
        //角色列表
        let roleList = roleStore.allRoles;

        // console.log(targetRoleid)
        // console.log('---current',currentRoleid)
        // console.log(roleList)

        let isChildRole = isChild(currentRoleid, targetRoleid, roleList);

        // console.log(isChildRole)

        //保存当前按钮的类名

        let className = el.className;

        if (!isChildRole) {
            //说明是父级角色
            el.className = className + ' is-disabled'
            //禁用按钮
            el.disabled = true;

            //禁用switch
            if (el.querySelector('.el-switch__core')) {
                el.querySelector('.el-switch__input').setAttribute('disabled', 'disabled');
                el.querySelector('.el-switch__core').classList.add('is-disabled')

            }

        }


        // if (children.find(item => item.roleid == targetRoleid)) {
        //     el.className = className + ' is-disabled'
        //     //禁用按钮
        //     el.disabled = true;
        // }


    }

}


export default {
    install: function (app) {
        app.directive('access', vAccess)
    }
}