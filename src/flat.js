//数据库中所有的数据都是扁平结构

//根节点 子节点

let flatData = [
    {
        id: 1, name: '教学部', parentid: 0,
        chdidren: [
            { id: 4, name: 'H5前端部门', parentid: 1 },
            { id: 5, name: 'java部门', parentid: 1 },
        ]
    },



    { id: 2, name: '行政部', parentid: 0 },
    { id: 3, name: '品保部', parentid: 0 },


    { id: 6, name: '班主任', parentid: 3 }
]


//视图呈现的时候会使用树形,children

//扁平数据转换成树形数据怎么转?

let faltToTree = (data) => {

    //声明一个变量,存储转换后的数据
    let tree = []

    //遍历扁平数据

    data.forEach(item => {
        //判断是否有父节点
        if (!item.parentid) {
            //没有父节点,说明是根节点,直接push到tree中
            tree.push(item)
        } else {
            //都是有parentid,都有父节点

            //找父节点

            let parent = data.find(v => v.id === item.parentid)

            //parent有没有children属性

            if (!parent.children) {
                parent.children = []
            }
            parent.children.push(item)
        }
    })

    return tree
}

let tree = faltToTree(flatData)

console.log(tree)

