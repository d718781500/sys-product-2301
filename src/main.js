import { createApp } from 'vue'
//引入状态管理pinia
import { createPinia } from "pinia"
//引入路由
import router from "./router/index"
import App from './App.vue'
import piniaPluginPersistedstate from "pinia-plugin-persistedstate"
//引入element-plus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//引入全局css文件

import "@/assets/styles/el-reset.css"

//引入公共样式

import "@/assets/styles/common.css"

//引入nprogress的css
import "nprogress/nprogress.css"

//引入menuStore

import useMenu from "@/store/menulist"

//引入usersStore

import useUsers from "@/store/users"

// import "./flat.js"
import directives from './directives/index.js'

// import "./utils/flexible.js"

let app = createApp(App)

app.use(ElementPlus)

let pinia = createPinia();
//Pinia使用持久化存储
pinia.use(piniaPluginPersistedstate);



//路由钩子(全局前置守卫)

router.beforeEach(async (to, from) => {
    //to目标路由对象
    //from 源路由对象
    // console.log('to---', to)
    // console.log('from---', from)

    //返回false 终止导航 页面不加载

    // if (to.path !== '/login') {
    //     return {
    //         path: "/login"
    //     }
    // }

    //用户如果登入了,那么允许访问其他页面

    //判断sessionStorage中是否有token

    let token = sessionStorage.getItem('tk');

    if (!token) {
        //用户如果没有登入,用户访问其他页面一律跳转到登入页

        //如果访问的是登入页,什么也不做,通过就行,如果不是,那么就要跳转回登入页

        if (to.path !== "/login") {
            return {
                path: "/login"
            }
        }

    } else {
        //已经登入 发起store中的请求,获取后台菜单数据的方法

        let menuStore = useMenu();

        //判断menuStore中是否获取了用户菜单

        if (menuStore.menulist.length < 1) {
            //没有计算出用户菜单
            //调用方法发起请求
            await menuStore.getMenuList()

            //这句代码的含义就是废弃掉之前的导航,因为之前的导航动态添加的路由还没有被添加进来,容易匹配成404
            //这段代码会重新激活路由匹配,匹配动态添加之后的内容
            // return { path: to.path, query: to.query, params: to.params }
            await router.replace(to.path)
        }

    }

})

//全局后置守卫

router.afterEach((to, from) => {
    let usersStore = useUsers()
    // console.log(usersStore.breadCrumbList)
    // console.log('after---to', to)
    // console.log('after---from', from)

    //这么写的目的是为了确保动态路由在废弃掉上一个404路由的时候,面包屑数据不被重新复制
    if (to.meta.key) {
        usersStore.breadCrumbList = to.matched.filter(item => {
            return item.path !== "/"
        })
    }

    // console.log(usersStore.breadCrumbList)

    //获取matched匹配到的数据,生成面包屑需要使用的数据
})
// 数组去重


//使用路由
app.use(router)
app.use(pinia)
app.use(directives)
app.mount('#app')
