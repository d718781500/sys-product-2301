export default {
    path: "/",
    component: () => import("@/layout/index.vue"),
    redirect:"/welcome",
    children: [],
}