// export default {
//     path: "/",
//     component: () => import("../layout/index.vue"),
//     children: 

// }

let routes = [
    {
        path: "welcome",
        component: () => import("../pages/home/welcome/index.vue"),
        meta: {
            key: "glsy",
            title: '管理首页',
            icon: "icon-shouye1"
        }
    },
    {
        path: "menus",
        component: () => import("../pages/home/menus/index.vue"),
        meta: {
            key: 'menus',
            title: '菜单管理',
            icon: "icon-caidan"
        }
    },
    {
        path: 'stu',
        component: () => import("../pages/home/stu/index.vue"),
        meta: {
            key: "xygl",
            title: "学员管理",
            icon: "icon-xueyuanguanli"
        },
        redirect: "/stu/product",
        children: [
            {
                path: "product",
                component: () => import('../pages/home/stu/product/index.vue'),
                meta: {
                    key: "xyxmgl",
                    title: "学员项目管理",
                    icon: "icon-jiazai",
                }
            },
            {
                path: "profile",
                component: () => import('../pages/home/stu/profile/index.vue'),
                meta: {
                    key: "xyzl",
                    title: "学员资料",
                    icon: "icon-ziliao",
                }
            },
            {
                path: "dormitory",
                component: () => import("../pages/home/stu/dormitory/index.vue"),
                meta: {
                    key: "xysh",
                    title: "学员宿舍",
                    icon: "icon-jianzhu_sushe",
                }
            }
        ]
    },
    {
        path: 'mine',
        component: () => import('../pages/home/mine/index.vue'),
        meta: {
            key: 'wdzx',
            title: '我的中心',
            icon: "icon-xingzhuanggongnengtubiao-"
        }
    },
    {
        path: "statistics",
        component: () => import('../pages/home/statistics/index.vue'),
        meta: {
            key: 'sjtj',
            title: '数据统计',
            icon: "icon-tongji"
        }
    },
    {
        path: "attendance",
        component: () => import('../pages/home/attendance/index.vue'),
        meta: {
            key: 'kxgl',
            title: '考勤管理',
            icon: "icon-kaoqin_kaoqin_yueqinguanli"
        }
    },
    {
        path: "users",
        component: () => import('../pages/home/users/index.vue'),
        meta: {
            key: 'yhgl',
            title: '用户管理',
            icon: "icon-yonghu1"
        }
    },
    {
        path: "permission",
        component: () => import("../pages/home/permission/index.vue"),
        meta:{
            key: 'permission',
            title: '权限管理',
            icon: "icon-ITquanxian"
        },
        children:[
            {
                path:"role",
                component:()=>import("../pages/home/permission/role/index.vue"),
                meta:{
                    key: 'role',
                    title: '角色管理',
                    icon: "icon-jiaosezuguanli"
                }
            }
        ]
    }
]

export default routes
