import { createRouter, createWebHashHistory } from "vue-router"

import dynamicRoutes from "./dynamicRoutes.js"


// console.log(dynamicRoutes)

//路由元信息 元就是可以随便定义

let routes = [
    {
        path: "/:page404(.*)*",
        component: () => import("../pages/page404/index.vue")
    },
    {
        path: "/login",
        component: () => import("../pages/login/index.vue")
    }
]

let router = createRouter({
    routes,
    history: createWebHashHistory()
})

export default router;