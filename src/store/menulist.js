import { defineStore } from "pinia"
import { ref } from "vue"

//引入路由

import router from "@/router/index.js"

import { getMenuListApi } from "@/api/permission"

//引入完整的动态路由表

import allRoutes from "@/router/dynamicRoutes"

//引入递归创建路由的方法

import recursionRoutes from "@/utils/recursionRoutes"

//引入commonRoutes
import commonRoutes from "../router/commonRoutes"

export default defineStore('menulist', () => {
    //控制菜单展开和收起的字段
    let isCollapse = ref(false)

    //菜单数据
    let menulist = ref([])

    //获取后台菜单数据的方法

    let getMenuList = () => new Promise(async (resolve, reject) => {
        // console.log(123)
        let result = await getMenuListApi()

        // menulist.value = result.data.menuList;

        // console.log(recursionRoutes)

        let userRoutes = recursionRoutes(allRoutes, result.data.data)

        commonRoutes.children = userRoutes;
        console.log(userRoutes)
        //将路由动态的添加到路由配置中 核心代码是addRoute

        router.addRoute(commonRoutes)

        //给menulist赋值

        menulist.value = userRoutes;
      

        resolve()
    })
    return { menulist, getMenuList, isCollapse }
})