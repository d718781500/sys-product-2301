import { defineStore } from "pinia"

import { getCurrentRolePermissionListApi } from "@/api/permission"
import { ref, computed } from "vue"
export default defineStore('permission', () => {
    //当前用户的权限
    let currentPermissions = ref([])

    //获取当前用户权限的方法

    let getCurrentPermissions = (currentRoleid) => {
        getCurrentRolePermissionListApi({ type: '1', roleid: currentRoleid }).then(res => {
            currentPermissions.value = res.data.data
        })
    }

    //将当前角色的权限数据映射成一个id数组

    let currentPermissionsIds = computed(() => {
        console.log(currentPermissions)
        return currentPermissions.value.map(item => item.id)
    })

    return { currentPermissions, getCurrentPermissions, currentPermissionsIds }
}, {
    persist: true
})