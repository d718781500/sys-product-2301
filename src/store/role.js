import { defineStore } from "pinia"

import { ref } from "vue"

//引入获取角色的接口

import * as api from "@/api/role"

export default defineStore('role', () => {

    //全部角色

    let allRoles = ref([])

    //声明一个方法,获取角色

    let getAllRoles = () => {
        api.getRoleApi().then(res => {
            allRoles.value = res.data.data
        })
    }

    return { allRoles, getAllRoles }
}, {
    persist: true
})