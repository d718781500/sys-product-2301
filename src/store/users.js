import { defineStore } from "pinia"
import { ref } from "vue"

export default defineStore('users', () => {
    //定义面包屑数据
    let breadCrumbList = ref([])
    //声明用户信息的state
    let userInfo = ref({})
    //用户的按钮权限
    let permission = ref({})
  

    return { userInfo, permission, breadCrumbList }
}, {
    //持久存储
    persist: true
})