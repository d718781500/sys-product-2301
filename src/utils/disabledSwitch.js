//引入userStore

import useUser from "@/store/users"

//引入角色store

import useRole from "@/store/role"
import isChild from "@/utils/isChild"

let disabledSwitch = (roleid) => {

    let userStore = useUser()

    let roleStore = useRole();
    // console.log(binding)
    //当前角色id
    let currentRoleid = userStore.userInfo.value.roleid
    //目标角色id
    let targetRoleid = roleid;
    //角色列表
    let roleList = roleStore.allRoles;

    let isChildRole = isChild(currentRoleid, targetRoleid, roleList);

    if (!isChildRole) {
        return true
    } else {
        return false
    }

}




export default disabledSwitch