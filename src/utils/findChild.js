
function findChildren(data, parentId) {
    const children = [];
    data.forEach(((item) => {
        if (item.parentid === parentId) {
            children.push(item);
            children.push(...findChildren(data, item.roleid))
        }
    }))
    return children;
}

export default findChildren