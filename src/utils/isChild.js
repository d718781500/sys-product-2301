
//用于判断当前操作的目标角色是不是登入用的上级角色

/**
 * 
 * @param {String} currentRoleid 当前登入角色的id
 * @param {String} targetRoleid 操作的目标角色的id
 * @param {[]any} roleList 完整的角色表
 */

let roleList = [
    {
        id: 1,
        name: '超级管理员',
        parentid: 0
    },
    {
        id: 5,
        name: '超级管理员儿子',
        parentid: 1
    },
    {
        id: 2,
        name: '管理员',
        parentid: 1
    },
    {
        id: 3,
        name: '普通用户',
        parentid: 2
    }
]

// let currentRoleid = 3;

// let targetRoleid = 2;

// let isParent = (currentRoleid, targetRoleid, roleList) => {
//     //先给个条件,如果当前角色id和目标角色id相等,那么就是自己操作自己,直接返回true
//     if (currentRoleid == targetRoleid) return true;
//     // console.log(roleList)
//     //先通过当前的角色id找到当前角色的数据
//     let currentRole = roleList.find(item => item.roleid === currentRoleid);

//     if (currentRole) {

//         return isParent(currentRole.parentid, targetRoleid, roleList)

//     } else {
//         return false
//     }
// }

//找孩子,判断传入的这个目标角色是不是自己的孩子,如果是自己的孩子return true

//currentRoleid当前登入角色的id 
//targetRoleid 是目标角色的id
//roleList 完整的角色表
let isChild = (currentRoleid, targetRoleid, roleList) => {

    //从完整的角色表roleList中根据目标角色id取出目标角色的数据对象

    let targetRole = roleList.find(item => item.roleid === targetRoleid)

    //声明一个布尔值,用于储存最终的判断结果,是不是自己的孩子

    let isChildFlag = false;

    //使用while循环,一直向上查找,直到节点没有parentid

    while (targetRole && targetRole.parentid) {
        //进行对比
        if (targetRole.parentid === currentRoleid) {
            //是自己的孩子
            isChildFlag = true;
            //跳出循环
            break;
        } else {
            //不是自己的孩子,继续向上查找
            targetRole = roleList.find(item => item.roleid === targetRole.parentid)
        }

    }
    //返回判断的结果
    return isChildFlag
}


export default isChild