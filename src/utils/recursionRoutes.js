//这个方法的作用是根据后台返回的菜单数据和本地的路由配置数据进行对比,对比之后得到用户的路由页面

/**
 * 
 * @param {Array} allRoutes 用户的完整路由表(需要进行权限控制的路由表)
 * @param {Array} menulist 后台返回的菜单数据
 */

export default function recursionRoutes(allRoutes = [], menulist = []) {
    // console.log(allRoutes)
    // console.log(menulist)
    //声明一个数组用于存放计算之后的用户路由

    let userRoutes = [];
    // console.log(menulist)
    console.log(allRoutes,menulist)
    //开始遍历和递归
    menulist.forEach(menuItem => {
        allRoutes.forEach(routeItem => {
            if (menuItem.name === routeItem.meta.title) {
                //判断是否有子菜单
                if (menuItem.children && menuItem.children.length > 0) {

                    routeItem.children = recursionRoutes(routeItem.children, menuItem.children)
                }

                userRoutes.push(routeItem)
            }
        })
    })

    return userRoutes
}