import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

//通过node的path处理这个问题

import path from "path"

let _resolve = (dir) => {
  return path.resolve(dir)
}
// console.log(_resolve('src'))

//环境文件:帮助我们在不同的环境下面,进行不同的项目配置

export default ({ mode }) => {
  //loadEvn这个方法用于获取当前环境文件的配置

  let env = loadEnv(mode, __dirname)
  // console.log('----env----', env)
  console.log(env.VITE_BASE_URL)
  return defineConfig({
    base: "./",
    plugins: [
      vue(),
    ],
    
    server: {
      port: 8080,
      proxy: {
        "/api": {
          target: env.VITE_BASE_URL,
          rewrite: path => path.replace(/^\/api/, "")
        }
      }
    },
    //构建配置
    build: {
      //配置项目打包之后输出文件夹的名字
      outDir: path.resolve(env.VITE_MODE)
    },
    resolve: {
      //配置别名
      alias: {
        //@这个符号就会指向src的绝对路径
        "@": _resolve('src')
      }
    }
  })
}
